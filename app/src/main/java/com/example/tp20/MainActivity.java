package com.example.tp20;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FloatingActionButton fab = findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(MainActivity.this, WineActivity.class);
                startActivity(intent);
                Snackbar.make(view, "Vin", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        WineDbHelper dbHelper = new WineDbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor result = dbHelper.fetchAllWines();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                R.layout.test,
                result, new String[] {
                WineDbHelper.COLUMN_NAME,
                WineDbHelper.COLUMN_WINE_REGION },
                new int[] { R.id.textView, R.id.textView2 }, 0);
        ListView lv = (ListView) findViewById(R.id.lstWine);

        lv.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
