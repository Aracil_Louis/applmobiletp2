package com.example.tp20;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class WineActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        EditText editTitle = findViewById(R.id.wineName);
        EditText editWineRegion = findViewById(R.id.editWineRegion);
        EditText editLoc = findViewById(R.id.editLoc);
        EditText editClimate = findViewById(R.id.editClimate);
        EditText editPlantedArea = findViewById(R.id.editPlantedArea);
        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(WineActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


    }
}

