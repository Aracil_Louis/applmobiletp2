package com.example.tp20;

import java.util.HashMap;

public class WineList {

    private static HashMap<String, Wine> hashMap = init();

    private static HashMap<String, Wine> init() {
        HashMap<String, Wine> res = new HashMap<>();

        res.put("Château-Neuf-Du-Pape", new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        res.put("Arbois", new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        res.put("Beaumes-De-Venise", new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        res.put("Bergerac", new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        res.put("Côte-De-Brouilly", new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        res.put("Muscadet", new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        res.put("Bandol", new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        res.put("Vouvray",new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        res.put("Ayze",new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        res.put("Meursault", new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        res.put("Ventoux",new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));

        return res;
    }

    public static String[] getNameArray() {
        return hashMap.keySet().toArray(new String[hashMap.size()]);
    }

    public static Wine getWine(String name) {
        return hashMap.get(name);
    }


}
